# Geothermal Data Dieng

Analisis statistik dan visualisasi untuk data geokimia gas, kondensat dan air kawah di Plato Dieng. Data ini merupakan bagian dari Riset S3 Parpar Priyatna (Badan Geologi) di Prodi S3 Teknik Geologi, Universitas Padjadjaran.

Kontribusi Dasapta Erwin Irawan: visualisasi data menggunakan R.